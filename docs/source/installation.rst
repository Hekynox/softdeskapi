Installation
============

Afin de déployer le projet en local vous pouvez suivre ces instructions :

**Installation de pipenv**

``pip install --user pipenv``

**Vérifier que pipenv est bien installé**

``pipenv --version``

Si une erreur est retourné lors de cette commande, cela signifie certainement que le chemin ou il a été installé ne se trouve pas dans votre variable d'environnement 'PATH', ajoutez le et réessayez.

**Clôner le projet, créer l'environnement virtuel et installer les dépendances**


``git clone https://gitlab.com/Hekynox/softdeskapi.git``

``cd softdeskapi/``

``pipenv install --dev``

**Activer l'environnement virtuel**

``pipenv shell``

**Créer la base de données et démarrer le serveur du projet en local**

``python manage.py makemigrations``

``python manage.py migrate``

``python manage.py runserver``