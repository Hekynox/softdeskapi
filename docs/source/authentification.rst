Authentification
================

.. _createaccount:

Créer un compte
---------------

Pour créer un compte, faite une requête POST à cette URL ``http://localhost:8000/api/users/`` avec le contenu suivant :

.. code-block:: json

    {
        "username": "nom_utilisateur",
        "password": "mot_de_passe",
        "age": 24,
        "consent_share": true,
        "consent_contact": false
    }

- "username" et "password" sont des chaînes de caractères.
- "age" est un nombre.
- "consent_share" et "consent_contact" sont des valeurs booléennes ('true' ou 'false')

Il est nécessaire d'avoir 15 ans minimum pour la création d'un compte.

.. _getJWT:

Obtenir un token d'authentification (JWT)
-----------------------------------------

Une fois votre compte créé, il est nécessaire de posséder un token d'authentification (Json Web Token),
pour l'obtenir effectuez une requête POST sur ``http://localhost:8000/api/token/`` avec le contenu suivant :

.. code-block:: json

    {
        "username": "votre_nom_utilisateur",
        "password": "votre_mot_de_passe"
    }

Réponse attendue : 

.. code-block:: json

    {
        "refresh": "token_refresh",
        "access": "token_access"
    }

Votre ``token_access`` devra être inclus dans l'entête (header) ``Authorization`` de vos requêtes en tant que jeton ``Bearer`` pour accéder
aux différents endpoint protégés de l'API, celui ci possède un temps de validité de **24 heures**.

.. _refreshJWT:

Rafraîchir son token d'authentification (JWT)
---------------------------------------------

Une fois votre ``token_access`` expiré (après 24 heures), vous pouvez en obtenir un nouveau à l'aide de votre ``token_refresh``, il vous suffit d'effectuer
une requêtes POST sur ``http://localhost:8000/api/token/refresh/`` avec le contenu suivant :

.. code-block:: json

    {
        "refresh": "token_refresh"
    }

