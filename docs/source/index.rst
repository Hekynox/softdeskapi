.. SoftdeskAPI documentation master file, created by
   sphinx-quickstart on Tue Jul 23 06:44:00 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SoftdeskAPI documentation
=========================


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation
   authentification
   api

