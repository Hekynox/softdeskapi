API
===

Permissions
-----------

.. _isnotauthenticated:

IsNotAuthenticated
~~~~~~~~~~~~~~~~~~

Permission pour utilisateur non authentifié.

.. autoclass:: authentication.permissions.IsNotAuthenticated

.. _iscontributor:

IsContributor
~~~~~~~~~~~~~

Permission pour les contributeurs.

.. autoclass:: authentication.permissions.IsContributor

.. _isselfonly:

IsSelfOnly
~~~~~~~~~~

Permission pour soi-même.

.. autoclass:: authentication.permissions.IsSelfOnly

.. _isadmin:

IsAdmin
~~~~~~~

Permission pour les administrateurs.

.. _isauthenticated:

IsAuthenticated
~~~~~~~~~~~~~~~

Permission pour les utilisateurs authentifiés.

Endpoints
---------

**api/token/**
~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/token/``

| Permet l'obtention d'un token d'accès. Voir :ref:`getJWT`

| Accepte les requêtes : ``POST``

**api/token/refresh/**
~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/token/refresh/``

| Permet de récupérer un nouveau token d'accès après expiration du précédent. Voir :ref:`refreshJWT`

| Accepte les requêtes : ``POST``

**admin/**
~~~~~~~~~~

url complet : ``http://localhost:8000/admin/``

Interface d'administration, il est nécessaire de posséder un compte administrateur pour s'authentifier.

**api/users/**
~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/users/``

| Permet de créer un compte. Voir :ref:`createaccount`

| Accepte les requêtes : ``POST``, ``GET``

| Permissions :

    - ``POST`` = :ref:`isnotauthenticated`
    - ``GET`` = :ref:`isadmin`

**api/users/my_account/**
~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/users/my_account/``

Endpoint de modification du compte de l'utilisateur. une requête effectué sur cette URL agira sur vos données de compte.

| Accepte les requêtes :  ``GET``, ``PATCH``, ``PUT``, ``DELETE``

| Permissions : 

    - :ref:`isselfonly`

**api/my_contributions/**
~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/my_contributions/``

Liste tous les projets auquel contribue l'utilisateur.

| Accepte les requêtes : ``GET``

Exemple de réponse après une requête ``GET`` : 

.. code-block:: json

    {
        "count": 3,
        "next": "http://localhost:8000/api/my_contributions/?limit=5&offset=5",
        "previous": null,
        "results": [
            {
                "project_title": "mon projet",
                "project_url": "http://localhost:8000/api/projects/1/",
                "join_date_time": "18-07-2024 11:38:26"
            },
            {
                "project_title": "projet 2",
                "project_url": "http://localhost:8000/api/projects/2/",
                "join_date_time": "21-07-2024 05:38:01"
            },
            {
                "project_title": "projet 3",
                "project_url": "http://localhost:8000/api/projects/3/",
                "join_date_time": "21-07-2024 05:38:19"
            }
        ]
    }


**api/my_issues/**
~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/my_issues/``

Liste toutes les issues appartenant à l'utilisateur.

| Accepte les requêtes : ``GET``

Exemple de réponse après une requête ``GET`` :

.. code-block:: json

    {
        "count": 7,
        "next": "http://localhost:8000/api/my_issues/?limit=5&offset=5",
        "previous": null,
        "results": [
            {
                "title": "issue 3",
                "description": "qhhee",
                "priority": "LOW",
                "tag": "BUG",
                "progress": "TODO",
                "issue_url": "http://localhost:8000/api/projects/1/issues/4/",
                "view_comments": "http://localhost:8000/api/projects/1/issues/4/comments/"
            },
            {
                "title": "Issue of project 2",
                "description": "Description issue",
                "priority": "LOW",
                "tag": "BUG",
                "progress": "TODO",
                "issue_url": "http://localhost:8000/api/projects/2/issues/6/",
                "view_comments": "http://localhost:8000/api/projects/2/issues/6/comments/"
            },
            {
                "title": "Issue of project 7",
                "description": "Description issue",
                "priority": "HIGH",
                "tag": "FEAT",
                "progress": "TODO",
                "issue_url": "http://localhost:8000/api/projects/2/issues/7/",
                "view_comments": "http://localhost:8000/api/projects/2/issues/7/comments/"
            }
        ]
    }

**api/my_comments/**
~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/my_comments/``

Liste tous les commentaires appartenant à l'utilisateur.

| Accepte les requêtes : ``GET``

Exemple de réponse après une requête ``GET`` :

.. code-block:: json

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "description": "j'ai résolu un probleme",
                "date_created": "18-07-2024 12:10:33",
                "comment_url": "http://localhost:8000/api/projects/1/issues/1/comments/1/"
            }
        ]
    }

**api/projects/**
~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/projects/``

Permet de lister tous les projets, et d'en créer un nouveau.

Accepte les requêtes : ``GET``, ``POST``

Exemple de réponse après une requête ``GET``:

.. code-block:: json

    {
        "count": 10,
        "next": "http://localhost:8000/api/projects/?limit=5&offset=5",
        "previous": null,
        "results": [
            {
                "title": "mon projet",
                "description": "ma desc",
                "type": "BE",
                "date_created": "18-07-2024 11:38:26",
                "project_url": "http://localhost:8000/api/projects/1/",
                "view_issues": "http://localhost:8000/api/projects/1/issues/"
            },
            {
                "title": "projet 2",
                "description": "desc of project 2",
                "type": "BE",
                "date_created": "21-07-2024 05:38:01",
                "project_url": "http://localhost:8000/api/projects/2/",
                "view_issues": "http://localhost:8000/api/projects/2/issues/"
            },
            {
                "title": "projet 3",
                "description": "desc of project 3",
                "type": "BE",
                "date_created": "21-07-2024 05:38:19",
                "project_url": "http://localhost:8000/api/projects/3/",
                "view_issues": "http://localhost:8000/api/projects/3/issues/"
            },
            {
                "title": "projet 4",
                "description": "desc of project 4",
                "type": "BE",
                "date_created": "21-07-2024 05:38:32",
                "project_url": "http://localhost:8000/api/projects/4/",
                "view_issues": "http://localhost:8000/api/projects/4/issues/"
            },
            {
                "title": "projet 5",
                "description": "desc of project 5",
                "type": "BE",
                "date_created": "21-07-2024 05:38:41",
                "project_url": "http://localhost:8000/api/projects/5/",
                "view_issues": "http://localhost:8000/api/projects/5/issues/"
            }
        ]
    }

- "project_url" contient l'url du projet.
- "view_issues" contient l'url pour accéder aux issues du projet.

**api/projects/project_id**
~~~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/projects/1/``

Permet d'afficher un projet, de le modifier ou de le supprimer.

Permissions :

    * ``GET`` = :ref:`isauthenticated`
    * ``PUT``, ``PATCH``, ``DELETE`` = :ref:`isselfonly`

Exemple de réponse après une requête ``GET`` :

.. code-block:: json

    {
        "title": "mon projet",
        "id_project": 1,
        "description": "ma desc",
        "type": "BE",
        "date_created": "18-07-2024 11:38:26",
        "author_username": "david",
        "contributors": [
            "david",
            "admin"
        ]
    }

**api/projects/project_id/issues/**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/projects/1/issues/``

Permet de lister toutes les issues d'un projet, et de créer une issue.

Accepte les requêtes : ``GET``, ``POST``

Exemple de réponse après une requête ``GET`` :

.. code-block:: json

    {
        "count": 2,
        "next": null,
        "previous": null,
        "results": [
            {
                "title": "issue 2",
                "description": "ldkdkjkd",
                "priority": "LOW",
                "tag": "BUG",
                "progress": "TODO",
                "issue_url": "http://localhost:8000/api/projects/1/issues/2/",
                "view_comments": "http://localhost:8000/api/projects/1/issues/2/comments/"
            },
            {
                "title": "issue 3",
                "description": "qhhee",
                "priority": "LOW",
                "tag": "BUG",
                "progress": "TODO",
                "issue_url": "http://localhost:8000/api/projects/1/issues/4/",
                "view_comments": "http://localhost:8000/api/projects/1/issues/4/comments/"
            }
        ]
    }

- "issue_url" contient l'url de l'issue.
- "view_comments" contient l'url pour accéder aux commentaires de l'issue.

**api/projects/project_id/issues/issue_id/**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/projects/1/issues/1/``

Permet d'afficher une issue, de la modifier ou de la supprimer.

Permissions : 

    * ``GET`` = :ref:`iscontributor`
    * ``PUT``, ``PATCH``, ``DELETE`` = :ref:`isselfonly`

Exemple de réponse après une requête ``GET`` : 

.. code-block:: json

    {
        "title": "issue 2",
        "description": "ldkdkjkd",
        "priority": "LOW",
        "tag": "BUG",
        "progress": "TODO",
        "date_created": "18-07-2024 11:42:47",
        "author_username": "david",
        "project_title": "mon projet",
        "assign_to": 2,
        "assigned_to_username": "admin",
        "issue_id": 2
    }

**api/projects/project_id/issues/issue_id/comments/**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/projects/1/issues/1/comments/``

Permet de lister tous les commentaires d’une issue, et de créer un commentaire.

Accepte les requêtes : ``GET``, ``POST``

Exemple de réponse après une requête ``GET`` :

.. code-block:: json

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "description": "j'ai résolu un probleme",
                "date_created": "18-07-2024 12:10:33",
                "comment_url": "http://localhost:8000/api/projects/1/issues/1/comments/1/"
            }
        ]
    }

- "comment_url" contient l'url du commentaire.

**api/projects/project_id/issues/issue_id/comments/comment_id/**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

url complet : ``http://localhost:8000/api/projects/1/issues/1/comments/1/``

Permet d'afficher un commentaire, de le modifier ou de le supprimer.

Permissions :

    * ``GET`` = :ref:`iscontributor`
    * ``PUT``, ``PATCH``, ``DELETE`` = :ref:`isselfonly`

Exemple de réponse après une requête ``GET`` :

.. code-block:: json

    {
        "description": "j'ai résolu un probleme",
        "date_created": "18-07-2024 12:10:33",
        "uuid": "de103422-ea27-479c-8472-886f1ff29701",
        "author_username": "david",
        "issue_url": "http://localhost:8000/api/projects/1/issues/1/"
    }

Projets
-------

Créer un projet
~~~~~~~~~~~~~~~

Faite une requête ``POST`` sur l'url ``http://localhost:8000/api/projects/`` avec le contenu suivant : 

.. code-block:: json

    {
        "title": "titre de votre projet",
        "description": "description de votre projet",
        "type": "FE"
    }

- "title" est une chaîne de caractères.
- "description" est une chaîne de caractères.
- "type" est une chaîne de caractères parmis ces 4 choix : ``BE``, ``FE``, ``iOS``, ``ANDRD``

.. code-block:: python

    class ProjectType(models.TextChoices):
        BACK_END = 'BE', 'Back-End'
        FRONT_END = 'FE', 'Front-End'
        IOS = 'iOS', 'iOS'
        ANDROID = 'ANDRD', 'Android'

Exemple de réponse après la requête : 

.. code-block:: json

    {
        "title": "titre de votre projet",
        "id_project": 11,
        "description": "description de votre projet",
        "type": "FE",
        "date_created": "23-07-2024 19:12:37",
        "author_username": "david",
        "contributors": [
            "david"
        ]
    }

Lors de la création d'un projet, vous en deviendrez automatiquement l'auteur ainsi que contributeur.

Modifier un projet
~~~~~~~~~~~~~~~~~~

Assurez vous d'être l'auteur du projet pour pouvoir le modifier.

Projet avant modification : 

.. code-block:: json

    {
        "title": "mon projet",
        "id_project": 1,
        "description": "ma desc",
        "type": "BE",
        "date_created": "18-07-2024 11:38:26",
        "author_username": "david",
        "contributors": [
            "david",
            "admin"
        ]
    }

Faite une requête ``PATCH`` si vous ne souhaitez modifier que partiellement le projet sur l'url de celui-ci, par exemple,
``http://localhost:8000/api/projects/1/`` avec le contenu que vous souhaitez modifier, ici nous allons modifier la description :

.. code-block:: json

    {
        "description": "Ma nouvelle description"
    }

Réponse de la requête : 

.. code-block:: json

    {
        "title": "mon projet",
        "id_project": 1,
        "description": "Ma nouvelle description",
        "type": "BE",
        "date_created": "18-07-2024 11:38:26",
        "author_username": "david",
        "contributors": [
            "david",
            "admin"
        ]
    }

Supprimer un projet
~~~~~~~~~~~~~~~~~~~

Assurez vous d'être l'auteur du projet pour pouvoir le supprimer.

Faite une requête ``DELETE`` sur l'url du projet, par exemple, ``http://localhost:8000/api/projects/1/``.

Si la suppression s'est effectué avec succès, le code HTTP : ``204 No Content`` sera retourné.

Devenir contributeur
~~~~~~~~~~~~~~~~~~~~

Pour devenir contributeur d'un projet vous devez vous munir de l'url du projet auquel vous souhaitez
contribuer puis y ajouter le chemin ``add_contributor/``.

Par exemple, pour devenir contributeur du projet ``http://localhost:8000/api/projects/2/``:

Faite une requête ``POST`` à l'url ``http://localhost:8000/api/projects/2/add_contributor/``

Issues
------

Créer une issue
~~~~~~~~~~~~~~~

Par exemple, si vous souhaitez créer une issue sur le projet ``http://localhost:8000/api/projects/2/`` :

Faite une requête ``POST`` sur l'url ``http://localhost:8000/api/projects/2/issues/`` avec le contenu suivant :

.. code-block:: json

    {
        "title": "titre de votre issue",
        "description": "description de votre issue",
        "priority": "MED",
        "tag": "BUG",
        "progress": "WIP"
    }

- "title" est une chaîne de caractères.
- "description" est une chaîne de caractères.
- "priority" est une chaîne de caractères parmis ces 4 choix : ``LOW``, ``MED``, ``HIGH``

.. code-block:: python

    class Priority(models.TextChoices):
        LOW = 'LOW', 'Low'
        MEDIUM = 'MED', 'Medium'
        HIGH = 'HIGH', 'High'

- "tag" est une chaîne de caractères parmis ces 4 choix : ``BUG``, ``FEAT``, ``TASK``

.. code-block:: python

    class Tag(models.TextChoices):
        BUG = 'BUG', 'Bug'
        FEATURE = 'FEAT', 'Feature'
        TASK = 'TASK', 'Task'

- "priority" est une chaîne de caractères parmis ces 4 choix : ``TODO``, ``WIP``, ``END``

.. code-block:: python

    class Progress(models.TextChoices):
        TODO = 'TODO', 'Todo'
        INPROGRESS = 'WIP', 'In Progress'
        FINISHED = 'END', 'Finished'

**Le champs "priority" n'est pas obligatoire, si celui ci n'est pas renseigné, il prendra "TODO" comme valeur par défaut.**

Exemple de la réponse après la requête :

.. code-block:: json

    {
        "title": "titre de votre issue",
        "description": "description de votre issue",
        "priority": "MED",
        "tag": "BUG",
        "progress": "WIP",
        "date_created": "24-07-2024 07:55:16",
        "author_username": "david",
        "project_title": "projet 2",
        "assign_to": null,
        "assigned_to_username": null,
        "issue_id": 10
    }

Modifier une issue
~~~~~~~~~~~~~~~~~~

Assurez vous d'être l'auteur de l'issue pour pouvoir la modifier.

Issue avant modification : 

.. code-block:: json

    {
        "title": "titre de votre issue",
        "description": "description de votre issue",
        "priority": "MED",
        "tag": "BUG",
        "progress": "WIP",
        "date_created": "24-07-2024 07:55:16",
        "author_username": "david",
        "project_title": "projet 2",
        "assign_to": null,
        "assigned_to_username": null,
        "issue_id": 10
    }

Faite une requête ``PATCH`` si vous ne souhaitez modifier que partiellement l'issue sur l'url de celui-ci, par exemple,
``http://localhost:8000/api/projects/2/issues/10/`` avec le contenu que vous souhaitez modifier, ici nous allons modifier la description :

.. code-block:: json

    {
        "description": "nouvelle description de l'issue"
    }

Réponse de la requêtes :

.. code-block:: json

    {
        "title": "titre de votre issue",
        "description": "nouvelle description de l'issue",
        "priority": "MED",
        "tag": "BUG",
        "progress": "WIP",
        "date_created": "24-07-2024 07:55:16",
        "author_username": "david",
        "project_title": "projet 2",
        "assign_to": null,
        "assigned_to_username": null,
        "issue_id": 10
    }

Supprimer une issue
~~~~~~~~~~~~~~~~~~~

Assurez vous d'être l'auteur du projet pour pouvoir le supprimer.

Faite une requête ``DELETE`` sur l'url de l'issue, par exemple, ``http://localhost:8000/api/projects/2/issues/10``.

Si la suppression s'est effectué avec succès, le code HTTP : ``204 No Content`` sera retourné.

Assigner une issue
~~~~~~~~~~~~~~~~~~

Il est possible d'assigner une issue à un autre contributeur du projet, pour cela vous devez être l'auteur de l'issue.

Dans cet exemple nous allons assigner l'issue à l'utilisateur "admin".

Issue avant assignation : 

.. code-block:: json

    {
        "title": "titre de votre issue",
        "description": "nouvelle description de l'issue",
        "priority": "MED",
        "tag": "BUG",
        "progress": "WIP",
        "date_created": "24-07-2024 07:55:16",
        "author_username": "david",
        "project_title": "projet 2",
        "assign_to": null,
        "assigned_to_username": null,
        "issue_id": 10
    }

Faite une requête ``PATCH`` sur l'url de l'issue avec le contenu suivant :

.. code-block:: json

    {
        "assign_to": "admin" 
    }

Réponse de la requête :

.. code-block:: json

    {
        "title": "titre de votre issue",
        "description": "nouvelle description de l'issue",
        "priority": "MED",
        "tag": "BUG",
        "progress": "WIP",
        "date_created": "24-07-2024 07:55:16",
        "author_username": "david",
        "project_title": "projet 2",
        "assign_to": 2,
        "assigned_to_username": "admin",
        "issue_id": 10
    }

Comments
--------

Créer un commentaire
~~~~~~~~~~~~~~~~~~~~~

Par exemple, si vous souhaitez créer un commentaire sur l'issue ``http://localhost:8000/api/projects/2/issues/10/``.

Faite une requête ``POST`` sur l'url ``http://localhost:8000/api/projects/2/issues/10/comments/`` avec le contenu suivant :

.. code-block:: json

    {
        "description": "Contenu de mon commentaire"
    }

- "description" est une chaîne de caractères.

Exemple de réponse après la requête :

.. code-block:: json

    {
        "description": "Contenu de mon commentaire",
        "date_created": "24-07-2024 08:08:10",
        "uuid": "8735a8bb-871e-4d05-82e2-a186fb266e4c",
        "author_username": "david",
        "issue_url": "http://localhost:8000/api/projects/2/issues/10/"
    }

- "issue_url" contient l'url de l'issue en relation avec votre commentaire.

Modifier un commentaire
~~~~~~~~~~~~~~~~~~~~~~~

Assurez vous d’être l’auteur du projet pour pouvoir le modifier.

Commentaire avant modification :

.. code-block:: json

    {
        "description": "Contenu de mon commentaire",
        "date_created": "24-07-2024 08:08:10",
        "uuid": "8735a8bb-871e-4d05-82e2-a186fb266e4c",
        "author_username": "david",
        "issue_url": "http://localhost:8000/api/projects/2/issues/10/"
    }

Faite une requête ``PATCH`` si vous ne souhaitez modifier que partiellement le commentaire sur l’url de celui-ci,
par exemple, ``http://localhost:8000/api/projects/2/issues/10/comments/2/`` avec le contenu que vous souhaitez modifier :

.. code-block:: json

    {
        "description": "Nouveau contenu de mon commentaire"
    }

Réponse de la requête :

.. code-block:: json

    {
        "description": "Nouveau contenu de mon commentaire",
        "date_created": "24-07-2024 08:08:10",
        "uuid": "8735a8bb-871e-4d05-82e2-a186fb266e4c",
        "author_username": "david",
        "issue_url": "http://localhost:8000/api/projects/2/issues/10/"
    }

Supprimer un commentaire
~~~~~~~~~~~~~~~~~~~~~~~~

Assurez vous d’être l’auteur du projet pour pouvoir le supprimer.

Faite une requête ``DELETE`` sur l’url du commentaire, par exemple, ``http://localhost:8000/api/projects/2/issues/10/comments/2/``.

Si la suppression s’est effectué avec succès, le code HTTP : 204 No Content sera retourné.
