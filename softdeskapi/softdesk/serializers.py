from rest_framework import serializers
from .models import Project, Issue, Comment, Contributor
from datetime import datetime
from django.contrib.auth import get_user_model


User = get_user_model()


class ProjectSerializer(serializers.ModelSerializer):
    """
    Serializer of Project model.

    Manage serialization and deserialization of project objects.

    Fields:
    - title(CharField): Title of project.
    - description(CharField): Description of project.
    - type(CharField): Type of project, type defined in ProjectType.
    - date_created(DateTimeField): Date & time when project has been created.
    - id_project(AutoField): ID instance of project.
    """
    date_created = serializers.SerializerMethodField()
    project_url = serializers.SerializerMethodField()
    view_issues = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = [
            'title',
            'description',
            'type',
            'date_created',
            'project_url',
            'view_issues'
        ]

    def get_date_created(self, obj):
        time_date = obj.time_created
        return time_date.strftime('%d-%m-%Y %H:%M:%S')

    def get_id_project(self, obj):
        return obj.id

    def get_project_url(self, obj):
        project_id = obj.id
        url = f'http://localhost:8000/api/projects/{project_id}/'
        return url

    def get_view_issues(self, obj):
        project_id = obj.id
        url = f'http://localhost:8000/api/projects/{project_id}/issues/'

        return url


class ProjectDetailSerializer(serializers.ModelSerializer):
    """
    Serializer of Project model.

    Manage serialization and deserialization of project objects with more
    details of it.

    Fields:
    - title(CharField): Title of project.
    - description(CharField): Description of project.
    - type(CharField): Type of project, type defined in ProjectType.
    - author_username(CharField): Username of user who created the project.
    - date_created(DateTimeField): Date & time when project has been created.
    - contributors(ManyToManyField): Users who contribute to the project.
    - id_project(AutoField): ID of project.
    """
    author_username = serializers.SerializerMethodField()
    contributors = serializers.SerializerMethodField()
    id_project = serializers.SerializerMethodField()
    date_created = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = [
            'title',
            'id_project',
            'description',
            'type',
            'date_created',
            'author_username',
            'contributors',
        ]

    def get_contributors(self, obj):
        contributors = obj.contributors.all()
        return [contrib.username for contrib in contributors]

    def get_author_username(self, obj):
        return obj.author.username

    def get_id_project(self, obj):
        return obj.id

    def get_date_created(self, obj):
        time_date = obj.time_created
        return time_date.strftime('%d-%m-%Y %H:%M:%S')


class IssueSerializer(serializers.ModelSerializer):
    """
    Serializer of Issue model.

    Manage serialization and deserialization of issue objects.

    Fields:
    - title(CharField): Title of issue.
    - description(CharField): Description of issue.
    - priority(CharField): Priority of issue. Priority defined in Priority.
    - tag(CharField): Tag of issue. Tag defined in Tag.
    - progress(CharField): Progression of issue. Progression defined in
    Progress.
    - issue_id(AutoFields): ID of Issue
    """
    issue_url = serializers.SerializerMethodField()
    view_comments = serializers.SerializerMethodField()

    class Meta:
        model = Issue
        fields = [
            'title',
            'description',
            'priority',
            'tag',
            'progress',
            'issue_url',
            'view_comments'
        ]

    def get_issue_url(self, obj):
        project_id = obj.project.id
        issue_id = obj.id
        url = 'http://localhost:8000/api/projects/{}/issues/{}/'.format(
            project_id, issue_id
        )
        return url

    def get_view_comments(self, obj):
        project_id = obj.project.id
        issue_id = obj.id

        base_url = 'http://localhost:8000/api/'
        url = base_url + 'projects/{}/issues/{}/comments/'.format(
            project_id, issue_id
        )
        return url


class IssueDetailSerializer(serializers.ModelSerializer):
    """
    Serializer of Issue model.

    Manage serialization and deserialization of issue objects with more
    details of it.

    Fields:
    - title(CharField): Title of issue.
    - description(CharField): Description of issue.
    - priority(CharField): Priority of issue. Priority defined in Priority.
    - tag(CharField): Tag of issue. Tag defined in Tag.
    - progress(CharField): Progression of issue. Progression defined in
    Progress.
    - time_created(DateTimeField): Date & Time when the issue has been
    created.
    - project_title(CharField): Title of related project.
    - author_username(CharField): Username of user who created the issue.
    - assign_to(ForeignKey): ID of the user to whom this issue has been
    assigned.
    - assigned_to_username(CharField): Username of user to whom this issue
    has been assigned.
    - issue_id(AutoField): ID of issue.
    """
    author_username = serializers.SerializerMethodField()
    project_title = serializers.SerializerMethodField()
    date_created = serializers.SerializerMethodField()
    assigned_to_username = serializers.SerializerMethodField()
    issue_id = serializers.SerializerMethodField()

    class Meta:
        model = Issue
        fields = [
            'title',
            'description',
            'priority',
            'tag',
            'progress',
            'date_created',
            'author_username',
            'project_title',
            'assign_to',
            'assigned_to_username',
            'issue_id',
        ]

    def get_author_username(self, obj):
        return obj.author.username

    def get_project_title(self, obj):
        return obj.project.title

    def get_date_created(self, obj):
        time_date = obj.time_created
        return time_date.strftime('%d-%m-%Y %H:%M:%S')

    def get_assigned_to_username(self, obj):
        if obj.assign_to:
            return obj.assign_to.username
        return None

    def get_issue_id(self, obj):
        return obj.id

    def to_internal_value(self, data):
        assign_to_username = data.get('assign_to')
        if assign_to_username:
            try:
                assign_to_user = User.objects.get(username=assign_to_username)
                data['assign_to'] = assign_to_user.id
            except User.DoesNotExist:
                raise serializers.ValidationError(
                    {'assign_to': 'User does not exist'}
                )

        return super().to_internal_value(data)


class CommentSerializer(serializers.ModelSerializer):
    """
    Serializer of Comment model.

    Manage serialization and deserialization of comment objects.

    Attributes:
    - author: display username of author instead of ID.

    Fields:
    - description(CharField): Content of comment.
    - date_created(DateTimeField): Date & Time when comment has been created.
    - id_comment(AutoField): ID of comment.
    """
    date_created = serializers.SerializerMethodField()
    comment_url = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'description',
            'date_created',
            'comment_url',
        ]

    def get_date_created(self, obj):
        time_date = obj.time_created
        return time_date.strftime('%d-%m-%Y %H:%M:%S')

    def get_comment_url(self, obj):
        project_id = obj.issue.project.id
        issue_id = obj.issue.id
        comment_id = obj.id

        url_base = 'http://localhost:8000/api/'
        url_comments = 'projects/{}/issues/{}/comments/{}/'.format(
            project_id, issue_id, comment_id
        )
        url = url_base + url_comments

        return url


class CommentDetailSerializer(serializers.ModelSerializer):
    """
    Serializer of Comment model.

    Manage serialization and deserialization of comment objects with more
    informations.

    Fields:
    - description(CharField): Content of comment.
    - date_created(DateTimeField): Date & Time when comment has been created.
    - uuid(UUIDField): UUID of comment.
    - issue_title(ForeignKey): Issue title to which the comment is related.
    - author_username(ForeignKey): Username of user who created the comment.
    - issue_url(CharField): Url of issue related.
    """
    author_username = serializers.SerializerMethodField()
    date_created = serializers.SerializerMethodField()
    issue_url = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'description',
            'date_created',
            'uuid',
            'author_username',
            'issue_url'
        ]

    def get_author_username(self, obj):
        return obj.author.username

    def get_date_created(self, obj):
        time_date = obj.time_created
        return time_date.strftime('%d-%m-%Y %H:%M:%S')

    def get_issue_url(self, obj):
        id_issue = obj.issue.id
        project_id = obj.issue.project.id
        url = "http://localhost:8000/api/projects/{}/issues/{}/".format(
            project_id, id_issue
        )
        return url


class ContributorSerializer(serializers.ModelSerializer):
    """
    Serializer of Contributor model.

    Manage serialization and deserialization of contributor objects.

    Serializer all fields of Contributor model.
    Fields:
    - project(ForeignKey): Project to which the contributor is related.
    - join_time(DateTimeField): Date & Time when user became of contributor of
    an project.
    - user_contrib(ForeignKey): User to which the contributor is related.
    """
    project_title = serializers.SerializerMethodField()
    user_contrib_username = serializers.SerializerMethodField()

    class Meta:
        model = Contributor
        fields = [
            'project',
            'join_time',
            'user_contrib',
            'user_contrib_username',
            'project_title'
        ]

    def get_project_title(self, obj):
        return obj.project.title

    def get_user_contrib_username(self, obj):
        return obj.user_contrib.username


class ContributorProjectSerializer(serializers.ModelSerializer):
    """
    Serializer of Contributor model.

    Manage serialization and deserialization of contributor objects.

    Fields:
    - project_title(CharField): Title of project to which contributor is
    related.
    - project_url(CharField): URL of project to which contributor is related.
    - join_date_time(DateTimeField): Date & Time when user bacame contributor
    of project.
    """
    project_title = serializers.SerializerMethodField()
    project_url = serializers.SerializerMethodField()
    join_date_time = serializers.SerializerMethodField()

    class Meta:
        model = Contributor
        fields = [
            'project_title',
            'project_url',
            'join_date_time'
        ]

    def get_project_title(self, obj):
        return obj.project.title

    def get_project_url(self, obj):
        project_id = obj.project.id
        url = f'http://localhost:8000/api/projects/{project_id}/'
        return url

    def get_join_date_time(self, obj):
        join_time = obj.join_time
        return join_time.strftime('%d-%m-%Y %H:%M:%S')
