import uuid
from django.db import models
from django.conf import settings


class Project(models.Model):
    """
    Model representing an project.

    Attributes:
        - title(CharField): Title of project.
        - description(CharField): Description of project.
        - type(CharField): Type of project, type defined in ProjectType.
        - author(ForeignKey): User who created the project.
        - time created(DateTimeField): Date & time when project has been
        created.
        - contributors(ManyToManyField): Users who contribute to the project.

    Class Attributes:
        - ProjectType(TextChoices): Choices to type of project. Choices :
            - 'BE' for Back-End
            - 'FE' for Front-End
            - 'iOS' for iOS
            - 'ANDRD' for Android
    """

    class ProjectType(models.TextChoices):
        BACK_END = 'BE', 'Back-End'
        FRONT_END = 'FE', 'Front-End'
        IOS = 'iOS', 'iOS'
        ANDROID = 'ANDRD', 'Android'

    title = models.CharField(max_length=128)
    description = models.CharField(max_length=4096, blank=True)
    type = models.CharField(max_length=5, choices=ProjectType)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    time_created = models.DateTimeField(auto_now_add=True)
    contributors = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through='Contributor',
        related_name='contributed_projects'
    )

    def __str__(self):
        return self.title


class Issue(models.Model):
    """
    Model representing an issue.

    Attributes:
    - title(CharField): Title of issue.
    - description(CharField): Description of issue.
    - priority(CharField): Priority of issue. Priority defined in Priority.
    - tag(CharField): Tag of issue. Tag defined in Tag.
    - progress(CharField): Progression of issue. Progression defined in
    Progress.
    - time_created(DateTimeField): Date & Time when the issue has been
    created.
    - project(ForeignKey): Project to which the issue is related.

    class Attributes:
        - Priority(TextChoices): Choices of issue priority. Choices :
            - 'LOW' for Low
            - 'MED' for Medium
            - 'HIGH' for High

        - Tag(TextChoices): Choices of tag issue. Choices :
            - 'BUG' for Bug
            - 'FEAT' for Feature
            - 'TASK' for Task

        - Progress(TextChoices): Choices of issue progression. Choices :
            - 'TODO' for Todo
            - 'WIP' for In Progress
            - 'END' for Finished
    """

    class Priority(models.TextChoices):
        LOW = 'LOW', 'Low'
        MEDIUM = 'MED', 'Medium'
        HIGH = 'HIGH', 'High'

    class Tag(models.TextChoices):
        BUG = 'BUG', 'Bug'
        FEATURE = 'FEAT', 'Feature'
        TASK = 'TASK', 'Task'

    class Progress(models.TextChoices):
        TODO = 'TODO', 'Todo'
        INPROGRESS = 'WIP', 'In Progress'
        FINISHED = 'END', 'Finished'

    title = models.CharField(max_length=128)
    description = models.CharField(max_length=4096, blank=True)
    priority = models.fields.CharField(max_length=4, choices=Priority)
    tag = models.fields.CharField(max_length=4, choices=Tag)
    progress = models.fields.CharField(max_length=4,
                                       choices=Progress,
                                       blank=True,
                                       default=Progress.TODO)
    time_created = models.DateTimeField(auto_now_add=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    assign_to = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
        blank=True, related_name='assigned_issue'
    )


class Comment(models.Model):
    """
    Model representing an comment.

    Attributes:
    - description(CharField): Content of comment.
    - time_created(DateTimeField): Date & Time when comment has been created.
    - uuid(UUIDField): Generate unique UUID for each comment.
    - issue(ForeignKey): Issue to which the comment is related.
    """
    description = models.CharField(max_length=4096, blank=False)
    time_created = models.DateTimeField(auto_now_add=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )


class Contributor(models.Model):
    """
    Model representing an contributor.

    Attributes:
    - project(ForeignKey): Project to which the contributor is related.
    - join_time(DateTimeField): Date & Time when user became of contributor of
    an project.
    - user_contrib(ForeignKey): User to which the contributor is related.

    Meta:
    - unique_together(tuple): Ensure for only one instance exist between a
    user and a project.
    """
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    join_time = models.DateTimeField(auto_now_add=True)
    user_contrib = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ('project', 'user_contrib')
