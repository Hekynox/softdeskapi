from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Project, Contributor


@receiver(post_save, sender=Project)
def add_contrib_when_project_creation(sender, instance, created, **kwargs):
    if created:
        Contributor.objects.create(project=instance,
                                   user_contrib=instance.author)
