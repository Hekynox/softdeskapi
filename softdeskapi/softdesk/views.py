from rest_framework import viewsets, status
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.decorators import action
from softdesk.serializers import (ProjectSerializer,
                                  IssueSerializer,
                                  CommentSerializer,
                                  ContributorSerializer,
                                  ProjectDetailSerializer,
                                  IssueDetailSerializer,
                                  CommentDetailSerializer,
                                  ContributorProjectSerializer)
from softdesk.models import Project, Issue, Comment, Contributor
from authentication.permissions import IsSelfOnly, IsContributor
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListAPIView


User = get_user_model()


class ProjectViewset(viewsets.ModelViewSet):
    """
    Viewset for viewing and editing projects.

    This viewset provides this actions:
    - create
    - retrieve
    - update
    - partial_update
    - destroy
    - list

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve projects objects.
    - serializer_class(Serializer): Serializer class used for serialize and
    deserialize project objects.
    - detail_serializer_class(Serializer): Serializer class used for serialize
    and deserialize projects objects with more details.

    Methods:
        - add_contributor:
            Use decorators action for add a contributor when user using post
            method on project detail, user who make this action will be add
            contributor on project.
        - get_permissions:
            Allows actions depending to who make this:
            - 'retrieve', 'create', 'list': Requires user authenticated.
            - 'update', 'partial-update', 'destroy': Requires user to be
            owner.
    """
    queryset = Project.objects.all().select_related(
        'author'
    ).prefetch_related(
        'contributors'
    )
    serializer_class = ProjectSerializer
    detail_serializer_class = ProjectDetailSerializer

    def get_queryset(self):
        return super().get_queryset()

    @action(detail=True, methods=['post'],
            permission_classes=[IsAuthenticated])
    def add_contributor(self, request, pk=None):
        project = self.get_object()

        contributor = Contributor.objects.create(
            project=project,
            user_contrib=request.user
        )

        serializer = ContributorSerializer(contributor)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def get_permissions(self):
        if self.action in ['retrieve', 'create', 'list']:
            self.permission_classes = [IsAuthenticated]
        if self.action in ['update', 'partial_update', 'destroy']:
            self.permission_classes = [IsSelfOnly]
        return super().get_permissions()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_serializer_class(self):
        if self.action in ['update', 'partial_update', 'retrieve', 'create',
                           'destroy']:
            return self.detail_serializer_class
        return super().get_serializer_class()


class IssueViewset(viewsets.ModelViewSet):
    """
    Viewset for viewing and editing issues.

    This viewset provides this actions:
    - create
    - retrieve
    - update
    - partial_update
    - destroy
    - list

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve issues objects.
    - serializer_class(Serializer): Serializer class used for serialize and
    deserialize issue objects.
    - detail_serializer_class(Serializer): Serializer class used for serialize
    and deserialize issue objects with more details.

    Methods:
        - get_permissions:
            Allows actions depending to who make this:
            - 'retrieve', 'create', 'list': Requires user contributor of
            project.
            - 'update', 'partial-update', 'destroy': Requires user to be
            owner.
    """
    queryset = Issue.objects.all()
    serializer_class = IssueSerializer
    detail_serializer_class = IssueDetailSerializer

    def get_queryset(self):
        project_id = self.kwargs['project_pk']
        queryset = Issue.objects.filter(project=project_id).select_related(
            'author', 'assign_to', 'project'
        )
        return queryset

    def get_permissions(self):
        if self.action in ['retrieve', 'create', 'list']:
            self.permission_classes = [IsContributor]
        if self.action in ['update', 'partial_update', 'destroy']:
            self.permission_classes = [IsSelfOnly]
        return super().get_permissions()

    def perform_create(self, serializer):
        project_id = self.kwargs['project_pk']
        project = Project.objects.get(id=project_id)
        serializer.save(author=self.request.user, project=project)

    def perform_update(self, serializer):
        project_id = self.kwargs['project_pk']
        project = Project.objects.get(id=project_id)
        assign_to = serializer.validated_data.get('assign_to')

        if assign_to and not project.contributors.filter(
                id=assign_to.id).exists():
            raise ValidationError("This user is not contributor of project" +
                                  " or does not exist")

        serializer.save()

    def get_serializer_class(self):
        if self.action in ['update', 'partial_update', 'retrieve', 'create',
                           'destroy']:
            return self.detail_serializer_class
        return super().get_serializer_class()


class CommentViewset(viewsets.ModelViewSet):
    """
    Viewset for viewing and editing comments.

    This viewset provides this actions:
    - create
    - retrieve
    - update
    - partial_update
    - destroy
    - list

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve comment objects.
    - serializer_class(Serializer): Serializer class used for serialize and
    deserialize comment objects.
    - detail_serializer_class(Serializer): Serializer class used for serialize
    and deserialize comment objects with more details.

    Methods:
        - get_permissions:
            Allows actions depending to who make this:
            - 'retrieve', 'create', 'list': Requires user contributor of
            project.
            - 'update', 'partial-update', 'destroy': Requires user to be
            owner.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    detail_serializer_class = CommentDetailSerializer

    def get_queryset(self):
        issue_id = self.kwargs['issue_pk']
        queryset = Comment.objects.filter(issue=issue_id).select_related(
            'author', 'issue__project'
        )
        return queryset

    def get_permissions(self):
        if self.action in ['retrieve', 'create', 'list']:
            self.permission_classes = [IsContributor]
        if self.action in ['update', 'partial_update', 'destroy']:
            self.permission_classes = [IsSelfOnly]
        return super().get_permissions()

    def perform_create(self, serializer):
        issue_id = self.kwargs['issue_pk']
        issue = Issue.objects.get(id=issue_id)
        serializer.save(author=self.request.user, issue=issue)

    def get_serializer_class(self):
        if self.action in ['update', 'partial_update', 'retrieve', 'create',
                           'destroy']:
            return self.detail_serializer_class
        return super().get_serializer_class()


class ContributorView(ListAPIView):
    """
    View for viewing of which projects user has contributor.

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve contributor
    objects.
    - serializer_class(Serializer): Serializer class used for serialize and
    deserialize contributor objects.
    """
    queryset = Contributor.objects.all()
    serializer_class = ContributorProjectSerializer

    def get_queryset(self):
        user = self.request.user
        return Contributor.objects.filter(user_contrib=user)


class MyIssueView(ListAPIView):
    """
    View for viewing issues owned by the user.

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve issue
    objects.
    - serializer_class(Serializer): Serializer class used for serialize and
    deserialize contributor objects.
    """
    queryset = Issue.objects.all()
    serializer_class = IssueSerializer

    def get_queryset(self):
        user = self.request.user
        return Issue.objects.filter(author=user)


class MyCommentsView(ListAPIView):
    """
    View for viewing comments owned by the user.

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve comment
    objects.
    - serializer_class(Serializer): Serializer class used for serialize and
    deserialize contributor objects.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get_queryset(self):
        user = self.request.user
        return Comment.objects.filter(author=user)
