import pytest
import uuid

from django.contrib.auth import get_user_model
from softdesk.models import Project, Contributor, Issue, Comment
from django.db.utils import IntegrityError


User = get_user_model()

##############################################################################
#   Fixtures
##############################################################################


@pytest.fixture
def user():
    return User.objects.create_user(
        username='testuser',
        password='passworduser',
        age=24,
        consent_share=True,
        consent_contact=True
        )


@pytest.fixture
def user2():
    return User.objects.create_user(
        username='user2',
        password='user2password',
        age=24,
        consent_share=True,
        consent_contact=True
    )


@pytest.fixture
def project(user):
    return Project.objects.create(
        title='project 0',
        description='description',
        type='BE',
        author=user
    )


@pytest.fixture
def issue(project, user):
    return Issue.objects.create(
        title='issue 0',
        description='description',
        priority='LOW',
        tag='BUG',
        project=project,
        author=user
    )


##############################################################################
#   softdesk.models.Project
##############################################################################
@pytest.mark.django_db
def test_create_project(user):
    """
    Testing to create new project
    """
    project = Project.objects.create(
        title='Project test',
        description='Description test',
        type='BE',
        author=user
    )

    assert project.title == 'Project test'
    assert project.description == 'Description test'
    assert project.type == 'BE'
    assert project.author == user
    assert project.time_created is not None
    assert Contributor.objects.filter(project=project,
                                      user_contrib=user).exists()
    assert user in project.contributors.all()


##############################################################################
#   softdesk.models.Issue
##############################################################################
@pytest.mark.django_db
def test_create_issue(project, user):
    """
    Testing to create an issue in project
    """
    issue = Issue.objects.create(
        title='Issue test',
        description='description test',
        priority='LOW',
        tag='BUG',
        project=project,
        author=user
    )

    assert issue.title == 'Issue test'
    assert issue.description == 'description test'
    assert issue.priority == 'LOW'
    assert issue.tag == 'BUG'
    assert issue.progress == 'TODO'
    assert issue.project == project
    assert issue.author == user
    assert issue.time_created is not None
    assert Contributor.objects.filter(project=project,
                                      user_contrib=user).exists()
    assert user in project.contributors.all()


##############################################################################
#   softdesk.models.Comment
##############################################################################
@pytest.mark.django_db
def test_create_comment(project, user, issue):
    comment = Comment.objects.create(
        description='description comment',
        issue=issue,
        author=user
    )

    assert comment.description == 'description comment'
    assert comment.time_created is not None
    assert comment.author == user
    assert comment.issue == issue
    assert isinstance(comment.uuid, uuid.UUID)
    assert Contributor.objects.filter(project=project,
                                      user_contrib=user).exists()
    assert user in project.contributors.all()


##############################################################################
#   softdesk.models.Contributor
##############################################################################
@pytest.mark.django_db
def test_contributor(project, user, user2):
    Contributor.objects.create(
        project=project,
        user_contrib=user2
    )

    with pytest.raises(IntegrityError):
        Contributor.objects.create(
            project=project,
            user_contrib=user
        )
