import pytest
import uuid
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from django.contrib.auth import get_user_model
from softdesk.models import Project, Issue, Comment


User = get_user_model()


##############################################################################
#   Fixtures
##############################################################################


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def user():
    return User.objects.create_user(
        username='testuser',
        password='passwordusertest',
        age=24,
        consent_share=True,
        consent_contact=True
    )


@pytest.fixture
def token_user(api_client, user):
    response = api_client.post('/api/token/', {
        "username": "testuser",
        "password": "passwordusertest"
    })
    assert response.status_code == 200
    token = response.json()
    return token['access']


@pytest.fixture
def user_contributor():
    return User.objects.create_user(
        username='usercontributor',
        password='passwordusercontributor',
        age=32,
        consent_share=True,
        consent_contact=True
    )


@pytest.fixture
def token_user_contrib(api_client, user_contributor):
    response = api_client.post('/api/token/', {
        "username": "usercontributor",
        "password": "passwordusercontributor"
    })
    token = response.json()
    return token['access']


@pytest.fixture
def user_not_contributor():
    return User.objects.create_user(
        username='usernotcontributor',
        password='passwordusernotcontributor',
        age=29,
        consent_share=True,
        consent_contact=True
    )


@pytest.fixture
def token_user_not_contrib(api_client, user_not_contributor):
    response = api_client.post('/api/token/', {
        "username": "usernotcontributor",
        "password": "passwordusernotcontributor"
    })
    token = response.json()
    return token['access']


@pytest.fixture
def project_test(user):
    return Project.objects.create(
        title='Projet 42',
        description='Projet servant aux tests',
        type='BE',
        author=user
    )


@pytest.fixture
def issue_test(user, project_test):
    return Issue.objects.create(
        title='Issue 12',
        description='Description de issue 12',
        priority='LOW',
        tag='BUG',
        project=project_test,
        author=user
    )


@pytest.fixture
def issue_test_many_contrib(user, project_many_contrib):
    return Issue.objects.create(
        title='Issue 14',
        description='Description of issue 14',
        priority='LOW',
        tag='BUG',
        project=project_many_contrib,
        author=user
    )


@pytest.fixture
def project_many_contrib(user, user_contributor):
    project = Project.objects.create(
        title='Projet 22',
        description='Projet with many contrib',
        type='BE',
        author=user,
    )
    project.contributors.add(user_contributor)

    return project


@pytest.fixture
def comment_test(issue_test, user):
    return Comment.objects.create(
        description='Comment of issue test',
        issue=issue_test,
        author=user
    )

##############################################################################
#   softdesk.views.ProjectViewset
##############################################################################


@pytest.mark.django_db
def test_listing_projects(api_client, user, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    response = api_client.get('/api/projects/')

    assert response.status_code == 200
    json_response = response.json()

    assert 'count' in json_response
    assert 'next' in json_response
    assert 'previous' in json_response
    assert 'results' in json_response
    assert isinstance(json_response['results'], list)


@pytest.mark.django_db
def test_listing_projects_user_not_authenticated(api_client):
    response = api_client.get('/api/projects/')

    assert response.status_code == 401


@pytest.mark.django_db
def test_create_project(api_client, user, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    project = {
        'title': 'Projet 24',
        'description': 'Description du projet 24',
        'type': 'BE',
    }

    response = api_client.post('/api/projects/', project, format='json')

    assert response.status_code == 201
    assert response.json()['title'] == 'Projet 24'


@pytest.mark.django_db
def test_create_project_user_not_authenticated(api_client, user):
    project = {
        'title': 'Projet 24',
        'description': 'Description du projet 24',
        'type': 'BE',
    }

    response = api_client.post('/api/projects/', project, format='json')

    assert response.status_code == 401


@pytest.mark.django_db
def test_modify_project(api_client, user, project_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    update_project = {
        'title': 'Projet 42 mis à jour',
        'description': 'Mise à jour du projet 42',
        'type': 'FE'
    }

    url = f'/api/projects/{project_test.id}/'
    response = api_client.patch(url, update_project, format='json')

    assert response.status_code == 200
    assert response.json()['title'] == 'Projet 42 mis à jour'
    assert response.json()['description'] == 'Mise à jour du projet 42'
    assert response.json()['type'] == 'FE'


@pytest.mark.django_db
def test_delete_project(api_client, user, project_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = f'/api/projects/{project_test.id}/'
    response = api_client.delete(url)
    response_get = api_client.get(url)

    assert response.status_code == 204
    assert response_get.status_code == 404
    assert not Project.objects.filter(id=project_test.id).exists()


@pytest.mark.django_db
def test_add_contributor_project(api_client, user_contributor, project_test,
                                 token_user_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user_contrib)

    url = f'/api/projects/{project_test.id}/add_contributor/'
    response = api_client.post(url)

    assert response.status_code == 201
    assert user_contributor in project_test.contributors.all()


@pytest.mark.django_db
def test_modify_project_by_not_owner_user(api_client,
                                          user_contributor,
                                          project_test,
                                          token_user_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user_contrib)

    update_project = {
        'title': 'Projet 42 mis à jour',
        'description': 'Mise à jour du projet 42',
        'type': 'FE'
    }

    url = f'/api/projects/{project_test.id}/'
    response = api_client.patch(url, update_project, format='json')
    print(project_test.author.username)
    print(user_contributor.username)

    assert response.status_code == 403
    assert project_test.author.username != user_contributor


@pytest.mark.django_db
def test_delete_project_by_not_owner(api_client,
                                     user_contributor,
                                     project_test,
                                     token_user_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user_contrib)

    url = f'/api/projects/{project_test.id}/'
    response = api_client.delete(url)

    assert response.status_code == 403
    assert Project.objects.filter(id=project_test.id).exists()


@pytest.mark.django_db
def test_add_contributor_not_authenticated(api_client, project_test):
    url = f'/api/projects/{project_test.id}/add_contributor/'
    response = api_client.post(url)

    assert response.status_code == 401


##############################################################################
#   softdesk.views.IssueViewset
##############################################################################


@pytest.mark.django_db
def test_listing_issues(api_client, user, project_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = f'/api/projects/{project_test.id}/issues/'
    response = api_client.get(url)

    assert response.status_code == 200
    json_response = response.json()

    assert 'count' in json_response
    assert 'next' in json_response
    assert 'previous' in json_response
    assert 'results' in json_response
    assert isinstance(json_response['results'], list)


@pytest.mark.django_db
def test_not_allow_user_to_listing_issue(api_client,
                                         user_not_contributor,
                                         project_test,
                                         token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    url = f'/api/projects/{project_test.id}/issues/'
    response = api_client.get(url)

    print(project_test.contributors.all())

    assert response.status_code == 403


@pytest.mark.django_db
def test_create_issues(api_client, user, project_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    issue_data = {
        'title': 'issue test create',
        'description': 'description test create',
        'priority': 'LOW',
        'tag': 'BUG',
        'project': project_test.id
    }
    url = f'/api/projects/{project_test.id}/issues/'
    response = api_client.post(url, issue_data, format='json')

    assert response.status_code == 201
    assert response.json()['title'] == issue_data['title']
    assert response.json()['progress'] == 'TODO'


@pytest.mark.django_db
def test_user_not_contrib_try_create_issue(api_client,
                                           project_test,
                                           user_not_contributor,
                                           token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    issue_data = {
        'title': 'issue test create',
        'description': 'description test create',
        'priority': 'LOW',
        'tag': 'BUG',
        'project': project_test.id,
        'author': user_not_contributor.id
    }

    url = f'/api/projects/{project_test.id}/issues/'
    response = api_client.post(url, issue_data, format='json')

    assert response.status_code == 403
    assert not Issue.objects.filter(id=1).exists()


@pytest.mark.django_db
def test_modify_issue(api_client, user, issue_test, project_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    issue_update = {
        'title': 'New title of issue',
        'progress': 'WIP'
    }

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/'
    response = api_client.patch(url, issue_update, format='json')

    assert response.status_code == 200
    assert response.json()['title'] == issue_update['title']
    assert response.json()['progress'] == issue_update['progress']


@pytest.mark.django_db
def test_modify_issue_by_not_owner_user(api_client,
                                        user_contributor,
                                        project_test,
                                        issue_test,
                                        token_user_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user_contrib)

    issue_update = {
        'title': 'New title of issue',
        'progress': 'WIP'
    }

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/'
    response = api_client.patch(url, issue_update, format='json')

    assert response.status_code == 403
    assert issue_test.title == 'Issue 12'
    assert issue_test.progress == 'TODO'


@pytest.mark.django_db
def test_delete_issue(api_client, issue_test, user, project_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/'
    response = api_client.delete(url)
    response_get = api_client.get(url)

    assert response.status_code == 204
    assert response_get.status_code == 404
    assert not Issue.objects.filter(id=issue_test.id).exists()


@pytest.mark.django_db
def test_delete_issue_by_not_owner_user(api_client,
                                        user_contributor,
                                        project_many_contrib,
                                        issue_test_many_contrib,
                                        token_user_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user_contrib)

    url = '/api/projects/{}/issues/{}/'.format(
        project_many_contrib.id, issue_test_many_contrib.id
    )
    response = api_client.delete(url)

    assert response.status_code == 403
    assert Issue.objects.filter(id=issue_test_many_contrib.id).exists()


@pytest.mark.django_db
def test_view_issue_detail(api_client,
                           user,
                           issue_test,
                           project_test,
                           token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/'
    response = api_client.get(url)

    assert response.status_code == 200


@pytest.mark.django_db
def test_view_issue_detail_by_not_allow_user(api_client,
                                             user_not_contributor,
                                             issue_test,
                                             project_test,
                                             token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/'
    response = api_client.get(url)

    assert response.status_code == 403


##############################################################################
#   softdesk.views.CommentViewset
##############################################################################
@pytest.mark.django_db
def test_listing_comment(api_client, user, project_test, issue_test,
                         token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/comments/'

    response = api_client.get(url)

    assert response.status_code == 200
    json_response = response.json()

    assert 'count' in json_response
    assert 'next' in json_response
    assert 'previous' in json_response
    assert 'results' in json_response
    assert isinstance(json_response['results'], list)


@pytest.mark.django_db
def test_listing_comment_by_not_contributor(api_client, project_test,
                                            issue_test, user_not_contributor,
                                            token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/comments/'

    response = api_client.get(url)

    assert response.status_code == 403


@pytest.mark.django_db
def test_create_comment(api_client, user, project_test, issue_test,
                        token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    comment_data = {
        'description': 'Comment of issue'
    }

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/comments/'

    response = api_client.post(url, comment_data, format='json')

    assert response.status_code == 201
    assert response.json()['description'] == comment_data['description']
    assert response.json()['author_username'] == user.username

    try:
        uuid.UUID(response.json()['uuid'], version=4)
    except ValueError:
        assert False


@pytest.mark.django_db
def test_create_comment_by_not_contributor(api_client, user_not_contributor,
                                           project_test, issue_test,
                                           token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    comment_data = {
        'description': 'Comment of issue',
        'issue': issue_test.id,
        'author': user_not_contributor.id
    }

    url = f'/api/projects/{project_test.id}/issues/{issue_test.id}/comments/'
    response = api_client.post(url, comment_data, format='json')

    assert response.status_code == 403
    assert not Comment.objects.filter(id=1).exists()


@pytest.mark.django_db
def test_modify_comment(api_client, user, project_test, issue_test,
                        comment_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    comment_update = {
        'description': 'New description issue'
    }

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_test.id, issue_test.id, comment_test.id
    )
    response = api_client.patch(url, comment_update, format='json')

    assert response.status_code == 200
    assert response.json()['description'] == comment_update['description']


@pytest.mark.django_db
def test_modify_comment_by_not_owner(api_client, user_not_contributor,
                                     project_test, issue_test,
                                     comment_test, token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    comment_update = {
        'description': 'New description issue'
    }

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_test.id, issue_test.id, comment_test.id
    )
    response = api_client.patch(url, comment_update, format='json')

    assert response.status_code == 403
    assert comment_test.description == 'Comment of issue test'


@pytest.mark.django_db
def test_delete_comment(api_client, user, project_test, issue_test,
                        comment_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_test.id, issue_test.id, comment_test.id
    )
    response = api_client.delete(url)
    response_get = api_client.get(url)

    assert response.status_code == 204
    assert response_get.status_code == 404
    assert not Comment.objects.filter(id=comment_test.id).exists()


@pytest.mark.django_db
def test_delete_comment_by_not_contributor(api_client, user_not_contributor,
                                           project_test, issue_test,
                                           comment_test,
                                           token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_test.id, issue_test.id, comment_test.id
    )
    response = api_client.delete(url)

    assert response.status_code == 403
    assert Comment.objects.filter(id=comment_test.id).exists()


@pytest.mark.django_db
def test_view_detail_comment(api_client, user, project_test, issue_test,
                             comment_test, token_user):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' + token_user)

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_test.id, issue_test.id, comment_test.id
    )
    response = api_client.get(url)

    assert response.status_code == 200


@pytest.mark.django_db
def test_view_detail_comment_by_not_contributor(api_client, project_test,
                                                user_not_contributor,
                                                issue_test, comment_test,
                                                token_user_not_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_not_contrib)

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_test.id, issue_test.id, comment_test.id
    )
    response = api_client.get(url)

    assert response.status_code == 403


@pytest.mark.django_db
def test_delete_comment_by_not_owner(api_client, user_contributor,
                                     issue_test, project_many_contrib,
                                     comment_test, token_user_contrib):
    api_client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                           token_user_contrib)

    url = '/api/projects/{}/issues/{}/comments/{}/'.format(
        project_many_contrib.id, issue_test.id, comment_test.id
    )
    response = api_client.delete(url)

    assert response.status_code == 403
    assert Comment.objects.filter(id=comment_test.id).exists()
