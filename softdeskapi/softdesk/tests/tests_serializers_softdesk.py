import pytest
import uuid

from django.contrib.auth import get_user_model

from softdesk.serializers import (ProjectSerializer,
                                  IssueSerializer,
                                  CommentSerializer)
from softdesk.models import Project, Issue
from rest_framework.test import APIClient


User = get_user_model()

##############################################################################
#   Fixtures
##############################################################################


@pytest.fixture
def user():
    return User.objects.create_user(
        username='testuser',
        password='passworduser',
        age=24,
        consent_share=True,
        consent_contact=True
        )


@pytest.fixture
def project_object(user):
    return Project.objects.create(
        title='Projet 0',
        description='description 0',
        type='BE',
        author=user
    )


@pytest.fixture
def project(user):
    return {
        'title': 'Projet 0',
        'description': 'description 0',
        'type': 'BE',
    }


@pytest.fixture
def issue_object(user, project_object):
    return Issue.objects.create(
        title='issue 0',
        description='description 0',
        priority='LOW',
        tag='BUG',
        project=project_object,
        author=user
    )


@pytest.fixture
def issue(project_object, user):
    return {
        'title': 'title issue',
        'description': 'description of issue',
        'priority': 'LOW',
        'tag': 'BUG'
    }


@pytest.fixture
def comment(issue_object, user):
    return {
        'description': 'description comment',
        'issue': issue_object.id
    }


##############################################################################
#   softdesk.serializers.ProjectSerializer
##############################################################################
@pytest.mark.django_db
def test_project_serializer_create(project, user):
    serializer = ProjectSerializer(data=project)
    assert serializer.is_valid(), serializer.errors
    project0 = serializer.save(author=user)

    assert project0.title == project['title']
    assert project0.description == project['description']
    assert project0.type == project['type']
    assert project0.author.username == user.username
    assert user in project0.contributors.all()
    assert project0.time_created is not None


@pytest.mark.django_db
def test_project_serializer_create_novalid(user):
    project = {
        'title': 'project 17',
        'description': 'project no valid',
        'type': 'TYPE NOT EXIST',
        'author': user.id,
    }
    serializer = ProjectSerializer(data=project)
    assert not serializer.is_valid()
    assert 'type' in serializer.errors


##############################################################################
#   softdesk.serializers.IssueSerializer
##############################################################################
@pytest.mark.django_db
def test_issue_serializer_create(issue, user, project_object):
    serializer = IssueSerializer(data=issue)
    assert serializer.is_valid(), serializer.errors
    issue0 = serializer.save(author=user, project=project_object)

    assert issue0.title == issue['title']
    assert issue0.description == issue['description']
    assert issue0.priority == issue['priority']
    assert issue0.tag == issue['tag']
    assert issue0.progress == 'TODO'
    assert issue0.author.username == user.username
    assert issue0.time_created is not None
    assert issue0.project.title == project_object.title


@pytest.mark.django_db
def test_issue_serializer_create_novalid(user, project_object):
    issue = {
        'title': 'Issue test',
        'description': 'Issue no valid',
        'priority': 'PRIORITY NOT EXIST',
        'tag': 'TAG NO EXIST',
        'project': project_object.id
    }
    serializer = IssueSerializer(data=issue)
    assert not serializer.is_valid()
    assert 'priority' in serializer.errors
    assert 'tag' in serializer.errors


##############################################################################
#   softdesk.serializers.CommentSerializer
##############################################################################
@pytest.mark.django_db
def test_comment_serializer_create(comment, user, issue_object):
    serializer = CommentSerializer(data=comment)
    assert serializer.is_valid(), serializer.errors
    comment0 = serializer.save(author=user, issue=issue_object)

    assert isinstance(comment0.uuid, uuid.UUID)
    assert comment0.description == comment['description']
    assert comment0.time_created is not None
    assert comment0.author.id == user.id
    assert comment0.issue.id == issue_object.id
