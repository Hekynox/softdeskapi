from django.contrib import admin
from softdesk import models


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'author', 'time_created')


class IssueAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'priority',
        'tag',
        'progress',
        'time_created',
        'project',
        'author')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('issue', 'time_created', 'uuid', 'author')


class ContributorAdmin(admin.ModelAdmin):
    list_display = ('project', 'user_contrib', 'join_time')


admin.site.register(models.Project, ProjectAdmin)
admin.site.register(models.Issue, IssueAdmin)
admin.site.register(models.Comment, CommentAdmin)
admin.site.register(models.Contributor, ContributorAdmin)
