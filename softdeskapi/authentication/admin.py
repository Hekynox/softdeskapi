from django.contrib import admin
from authentication.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'age', 'consent_share', 'consent_contact')


admin.site.register(User, UserAdmin)
