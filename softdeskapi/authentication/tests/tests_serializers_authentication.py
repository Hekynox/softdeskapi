import pytest

from django.contrib.auth import get_user_model

from authentication.serializers import UserSerializer


User = get_user_model()


@pytest.fixture
def data_user():
    return {
        'username': 'hekytest',
        'password': 'testpassword',
        'age': 18,
        'consent_contact': True,
        'consent_share': True
    }


@pytest.mark.django_db
def test_user_serializer_create(data_user):
    """
    Testing serializer to create new user
    """
    serializer = UserSerializer(data=data_user)
    assert serializer.is_valid()
    user = serializer.save()

    assert user.username == data_user['username']
    assert user.age == data_user['age']
    assert user.consent_share == data_user['consent_share']
    assert user.consent_contact == data_user['consent_contact']
    assert user.check_password(data_user['password'])


@pytest.mark.django_db
def test_user_serializer_validation_invalid_age(data_user):
    """
    Testing validation of serializer with no valid age.
    """
    data_user['age'] = -1

    serializer = UserSerializer(data=data_user)
    assert not serializer.is_valid()
    assert 'age' in serializer.errors
