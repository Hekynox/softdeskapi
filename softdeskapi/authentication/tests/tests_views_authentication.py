import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from django.contrib.auth import get_user_model

User = get_user_model()


##############################################################################
#   Fixtures
##############################################################################


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def user_data():
    return {
        'username': 'pytest_user',
        'password': 'pytestpassword',
        'age': 28,
        'consent_share': True,
        'consent_contact': True
    }


@pytest.fixture
def user_data_test():
    return {
        'username': 'pytest5',
        'password': 'pytestpassword8',
        'age': 28,
        'consent_share': True,
        'consent_contact': True
    }


@pytest.fixture
def user_data_nomajor():
    return {
        'username': 'user_nomajor',
        'password': 'userpassword',
        'age': 14,
        'consent_share': True,
        'consent_contact': True
    }


##############################################################################
#   authentication.views.User
##############################################################################
@pytest.mark.django_db
def test_create_user(api_client, user_data):
    url = '/api/users/'
    response = api_client.post(url, user_data, format='json')

    assert response.status_code == status.HTTP_201_CREATED
    assert User.objects.count() == 1
    user = User.objects.get(username=user_data['username'])
    assert user.username == user_data['username']
    assert user.age == user_data['age']
    assert user.consent_contact == user_data['consent_contact']
    assert user.consent_share == user_data['consent_share']
    assert user.check_password(user_data['password'])


@pytest.mark.django_db
def test_create_user_nomajor(api_client, user_data_nomajor):
    url = '/api/users/'
    response = api_client.post(url, user_data_nomajor, format='json')

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert User.objects.count() == 0


@pytest.mark.django_db
def test_create_user_when_authenticated(api_client, user_data, user_data_test):
    user = User.objects.create(**user_data)
    api_client.force_authenticate(user=user)
    url = '/api/users/'
    response = api_client.post(url, user_data_test, format='json')

    assert response.status_code == status.HTTP_403_FORBIDDEN
