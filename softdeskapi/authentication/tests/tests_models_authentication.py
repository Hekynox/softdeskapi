import pytest

from django.contrib.auth import get_user_model


User = get_user_model()


@pytest.mark.django_db
def test_create_user():
    """
    Testing to create new user.
    """
    user = User.objects.create_user(
        username='hekytest',
        password='monpasswordtest',
        age=18,
        consent_share=True,
        consent_contact=True,
    )
    assert user.username == 'hekytest'
    assert user.age == 18
    assert user.consent_share is True
    assert user.consent_contact is True
    assert user.check_password('monpasswordtest')


@pytest.mark.django_db
def test_create_superuser():
    """
    Testing to create superuser.
    """
    superuser = User.objects.create_superuser(
        username='admin',
        password='adminpassword',
        age=30
    )
    assert superuser.username == 'admin'
    assert superuser.check_password('adminpassword')
    assert superuser.age == 30
    assert superuser.is_active is True
    assert superuser.is_staff is True
