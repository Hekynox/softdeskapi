from rest_framework import permissions
from django.contrib.auth.models import AnonymousUser
from softdesk.models import Project, Issue, Comment


class IsSelfOnly(permissions.BasePermission):
    """
    Custom permissions.

    Allow only for it self.
    """

    def has_object_permission(self, request, view, obj):
        return obj == request.user or obj.author == request.user


class IsNotAuthenticated(permissions.BasePermission):
    """
    Custom permissions.

    Allow only if user is not authenticated.
    """

    def has_permission(self, request, view):
        return not request.user or isinstance(request.user, AnonymousUser)


class IsContributor(permissions.BasePermission):
    """
    Custom permission.

    Allow only a user contributor of a project.
    """
    def has_permission(self, request, view):
        project_id = view.kwargs.get('project_pk')
        if project_id:
            project = Project.objects.get(pk=project_id)
            return request.user in project.contributors.all()
        return False

    def has_object_permission(self, request, view, obj):
        if isinstance(obj, Project):
            return request.user in obj.contributors.all()
        elif isinstance(obj, Issue):
            return request.user in obj.project.contributors.all()
        elif isinstance(obj, Comment):
            return request.user in obj.issue.project.contributors.all()
        else:
            return False
