from rest_framework import serializers

from authentication.models import User


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer of User model.

    manage serialization and deserialization of user objects.

    Fields:
    - password: write-only field for user password.
    - consent_share: Boolean field indicating user's consent to share data.
    - consent_contact: Booleand field indicating user's consent to be
    contacted.

    Attributes:
    - model: The serializer model.
    - fields: List of fields to serialize/deserialize (all fields of model).

    Methods:
    - create(validated_data): Create new user instance based on validated
    data.
    """
    password = serializers.CharField(write_only=True, required=False)
    consent_share = serializers.BooleanField(required=False)
    consent_contact = serializers.BooleanField(required=False)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'password',
            'age',
            'consent_share',
            'consent_contact'
        ]

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        user = User.objects.create(**validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user
