from rest_framework import viewsets
from django.contrib.auth import get_user_model
from authentication.serializers import UserSerializer
from authentication.permissions import IsSelfOnly, IsNotAuthenticated
from rest_framework.permissions import IsAdminUser
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import MethodNotAllowed

User = get_user_model()


class UserViewset(viewsets.ModelViewSet):
    """
    Viewset for viewing and editing Users.

    This viewset provides this actions :
    - create
    - retrieve
    - update
    - partial_update
    - destroy
    - list

    Attributes:
    - queryset(QuerySet): Queryset will be used for retrieve user objects.
    - serializer_class(Serializer): Serializer class used for
    serialize dans deserialize objects.

    Methods:
        - get_permissions:
            Allow actions depending to who make this.
            - 'update', 'partial_update', 'destroy', 'retrieve': Requires
            user to be owner.
            - 'create' : Requires an user to be not authenticated
            - 'list': Requires an user with admin status.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=False, methods=['get', 'put', 'patch', 'delete'],
            permission_classes=[IsSelfOnly], url_path='my_account')
    def get_user_account(self, request):
        user = request.user

        if request.method == 'GET':
            serializer = self.get_serializer(user)
            return Response(serializer.data)

        elif request.method in ['PUT', 'PATCH']:
            serializer = self.get_serializer(user, data=request.data,
                                             partial=(
                                                 request.method == 'PATCH'
                                                 ))
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)

        elif request.method == 'DELETE':
            user.delete()
            return Response(status=204)

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsNotAuthenticated]
        if self.action == 'list':
            self.permission_classes = [IsAdminUser]
        if self.action == 'get_user_account':
            self.permission_classes = [IsSelfOnly]
        return super().get_permissions()

    def update(self, request, *args, **kwargs):
        raise MethodNotAllowed('PUT')

    def partial_update(self, request, *args, **kwargs):
        raise MethodNotAllowed('PATCH')

    def destroy(self, request, *args, **kwargs):
        raise MethodNotAllowed('DELETE')

    def retrieve(self, request, *args, **kwargs):
        raise MethodNotAllowed('GET')
