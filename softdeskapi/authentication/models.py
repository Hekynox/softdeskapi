from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)


class UserManager(BaseUserManager):
    """
    Custom manager for User model.

    This manager provides methods to create users and superusers and retrieves
    it by their natural keys.

    Methods:
    - create_user(
    username, password, age, consent_contact, consent_share, **extra_fields
    ):
    Create and save user with given username, password, age, consent of contact
    and consent to share data and extra fields.

    - create_superuser(username, password, **extra_fields):
    Create and save super user with given username, password and extra fields.

    - get_by_natural_key(username): Retrieves user instance by username.
    """
    def create_user(
            self, username, password=None, age=None, consent_contact=False,
            consent_share=False, **extra_fields
            ):
        if not username:
            raise ValueError("Vous devez indiquer un nom d'utilisateur.")
        user = self.model(
            username=username,
            **extra_fields,
            age=age,
            consent_contact=consent_contact,
            consent_share=consent_share,
            )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('age', 999)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(
                "Le super user doit avoir accès au site d'administration")
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(
                'Le super user doit avoir toutes les permissions')

        return self.create_user(username, password, **extra_fields)

    def get_by_natural_key(self, username):
        return self.get(username=username)


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user model for registrated user.

    This model extends 'AbstractBaseUser' and 'PermissionsMixin' for customize
    the default user model.

    Attributes:
    - username(str): Charfield for username.
    - is_staff(BooleanField): Define if user is allow to access admin site.
    - is_active(BooleanField): Define if user account is active.
    - age(Int): IntegerField for age of users.
    - consent_contact(BooleanField): Define if user allow to be contacted.
    - consent_share(BooleanField): Define if user allow to share your data.
    - objects: Instance of 'UserManager'.

    Methods:
    - __str__(): Returns string of user instance, displaying username.
    - natural_key(): Returns username of user.
    - save(): Switch variable on False if age of user under 15 years old and
    save this.
    """
    username = models.CharField(
        max_length=25, verbose_name="Nom d'utilisateur", unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    age = models.IntegerField(blank=False,
                              verbose_name="Age",
                              validators=[MinValueValidator(15)])
    consent_contact = models.BooleanField(
        default=False,
        verbose_name="Consentement pour être contacté",
        blank=False)
    consent_share = models.BooleanField(
        default=False,
        verbose_name="Consentement à partager vos données",
        blank=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username

    def natural_key(self):
        return (self.username)
