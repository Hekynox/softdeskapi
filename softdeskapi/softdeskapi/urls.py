"""
URL configuration for softdeskapi project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from authentication.views import UserViewset
from rest_framework_nested import routers
from rest_framework_simplejwt.views import (TokenObtainPairView,
                                            TokenRefreshView)
from softdesk.views import (ProjectViewset,
                            IssueViewset,
                            CommentViewset,
                            ContributorView,
                            MyIssueView,
                            MyCommentsView)


router = routers.SimpleRouter()
router.register('users', UserViewset, basename='users')
router.register('projects', ProjectViewset, basename='projects')

projects_router = routers.NestedSimpleRouter(router, 'projects',
                                             lookup='project')
projects_router.register('issues', IssueViewset, basename='project-issue')

issues_router = routers.NestedSimpleRouter(projects_router, 'issues',
                                           lookup='issue')
issues_router.register('comments', CommentViewset, basename='issue-comment')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls)),
    path('api/', include(projects_router.urls)),
    path('api/', include(issues_router.urls)),
    path('api/token/',
         TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('api/token/refresh/',
         TokenRefreshView.as_view(),
         name='token_refresh'),
    path('api/my_contributions/', ContributorView.as_view(),
         name='my-contributions'),
    path('api/my_issues/', MyIssueView.as_view(), name='my-issues'),
    path('api/my_comments/', MyCommentsView.as_view(), name='my-comments')
]
