[![Documentation Status](https://readthedocs.org/projects/softdeskapi/badge/?version=latest)](https://softdeskapi.readthedocs.io/fr/latest/)

# SoftDeskAPI

SoftDeskAPI est une API destinée à la création de projets, leurs suivis ainsi que leurs contributions, pour permettre cela, plusieurs fonctionnalités sont mises à dispositions des contributeurs tel que les issues et les commentaires.

Celle ci n'est accessible que pour les utilisateurs inscrits et authentifiés. 

## Utilisateurs

Un utilisateur doit obligatoirement être inscrit pour accéder à l'API, si ce n'est pas le cas il lui est possible de procéder à son inscription, il doit alors renseigner les champs suivants :

* Pseudonyme
* Mot de passe
* Age
* Consentement à être contacté
* Consentement à partager ses données

⚠ - **Un utilisateur de moins de 15 ans ne pourra pas valider son inscription**

Il lui est possible de modifier à tout moment ses données personnelles ou de les supprimer, si tel est le cas, aucune de ses données ne subsistera dans l'application.

Un utilisateur inscrit et qui s'identifie se voit attribuer un token (JWT: JSON Web Token), celui ci permettra à l'utilisateur d'être authentifié dans chaque requête qu'il effectuera, dès lors, il peut :

* Créer un projet (Devient contributeur et auteur du projet)
* Contribuer à un projet déjà existant en en devenant contributeur
* Créer des issues dans les projets auxquels il contribue
* Créer des commentaire dans les issues des projets auxquels il contribue

Afin de pouvoir effectuer toutes actions sur un projet, l'utilisateur doit obligatoire en être contributeur, dans le cas contraire il ne pourra effectuer aucune action.

## Contributeurs

Un utilisateur peut devevnir contributeur de n'importe quel projet, mais doit obligatoirement en devenir contributeur pour réaliser toutes actions sur un projet, dès lors il lui sera possible de créer des issues et des commentaires aux projets auxquelles il est lié.

## Projets

Un projet peut être créé par un utilisateur, ainsi il en devient l'auteur, et automatiquement un contributeur.

Afin de créer un projet, plusieurs champs doivent être renseigné lors de sa création : 

* Titre
* Description
* Type (Back-end, Front-end, iOS, Android)

L'auteur et la date de création étant automatiquement assigné au projet, les autres utilisateurs de l'API peuvent devenir contributeur de ce projet.

## Issues

Une issue ne peut être créée et liée qu'à un seul projet, et par un contributeur de celui ci, il est également possible à l'auteur de l'issue d'attribuer celle ci à un autre contributeur.

Une issue se compose de la sorte : 

* Titre
* Description
* Priorité (LOW, MEDIUM, HIGH)
* Tag (BUG, FEATURE, TASK)
* Statut de progression (To Do, In Progress, Finished)

A sa création, l'auteur étant automatiquement attribué à l'issue qu'il vient de créer, une date de création l'est également, et par défaut le statut de progression d'une issue lors de sa création est '**To Do**'

## Commentaires

Un commentaire ne peut être créé que dans une issue, et nécessite d'être contributeur du projet à laquelle l'issue appartient.

Lorsqu'un contributeur créé un commentaire dans une issue, une date de création lui est automatiquement associé, ainsi qu'un **uuid** et un auteur.

Un commentaire comprend :

* Une description
* L'issue à laquelle il est lié

# Documentation

Pour configurer le projet et utiliser l'API, référez vous à la documentation que vous trouverez en cliquant sur le badge en haut de ce README, ou cliquez sur 
ce lien : 

[Documentation](https://softdeskapi.readthedocs.io/fr/latest/)
